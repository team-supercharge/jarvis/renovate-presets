# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2023-06-16)


### Features

* base and api-generator configs ([11533d2](https://gitlab.com/team-supercharge/jarvis/renovate-presets/commit/11533d29cfd07a0685b4a6699093130b8ca4abde))
* initialize project with README ([f3154f9](https://gitlab.com/team-supercharge/jarvis/renovate-presets/commit/f3154f98f3c8c939526bd19970f55b4d7b520ded))
