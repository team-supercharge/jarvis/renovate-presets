# Renovate Presets

Presets for configuring [Renovate Bot](https://renovatebot.com) when used with JARVIS. The presets follow an **opt-in** approach for package updates: by default it disables updates for all packages, and selectively enables the ones that are going to be updated.

* [Getting Started](#getting-started)
* [Presets](#presets)
  * [`base` preset](#base-preset)
  * [`api-generator` preset](#api-generator-preset)
* [Contributing](#contributing)
* [Versioning](#versioning)

# Getting Started

As a first step [Renovate](https://renovatebot.com) need to be configured for your self-hosted GitLab instance or GitLab.com group following the [relevant section](https://gitlab.com/team-supercharge/jarvis/jarvis#renovate-templates) in the main JARVIS documentation.

The presets can be added to your project's `renovate.json` (or any [other supported](https://docs.renovatebot.com/configuration-options/)) file with the following syntax (mind the double slash, it is only correct this way):

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>team-supercharge/jarvis/renovate-presets//<PRESET_NAME>#<VERSION>",
  ],
  "<PROJECT-RULES>"
}
```

In practice the `base` preset should always be added first, followed by optionally a platform-specific template. Full example for an "API Generator" type project using [OASg](https://gitlab.com/team-supercharge/oasg) the config would look like:

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>team-supercharge/jarvis/renovate-presets//base#v1.0.0",
    "gitlab>team-supercharge/jarvis/renovate-presets//api-generator#v1.0.0"
  ]
}
```

> ⚠️ The version number for presets is advised to be kept in sync, but the `base` preset itself handles automatic version updates, so you should not worry about it too much.

---

# Presets

## `base` preset

This preset takes care of the general configuration that can be applied for all projects:

* enables the [Dependency Dashboard](https://docs.renovatebot.com/key-concepts/dashboard/) - for this "Issues" need to be enabled in GitLab project settings
* applies general commit configuration (prefix, etc)
* disables limit for MR creation (not needed in the opt-in approach)
* see the source of [base.json](base.json) for more details

and handles the dependencies:

* `team-supercharge/jarvis/jarvis`
  - updates JARVIS version in your `.gitlab-ci.yml` either in the [`include:project`](https://docs.gitlab.com/ee/ci/yaml/#includeproject), the [`include:remote`](https://docs.gitlab.com/ee/ci/yaml/#includeremote) or the simplified `include: "https://..."` syntax
* `team-supercharge/jarvis/renovate-presets`
  - update the version of presets stored in this repository in your [Renovate configuration file](https://docs.renovatebot.com/configuration-options/) (typically `renovate.json`)

&nbsp;

## `api-generator` preset

This preset takes care of the dependency updates of a typical "API Generator" type of project, and handles the following dependencies:

* `team-supercharge/oasg`
  - updates [OASg](https://gitlab.com/team-supercharge/oasg) version in both the `package.json` and in the `.gitlab-ci.yml` pipeline configuration as well and groups them in a single MR

---

# Contributing

If you have a new idea don't be afraid to share it by submitting an MR or asking in the #sc-ci Slack channel!

For open issues and improvement ideas lease visit the [Issues](https://gitlab.com/team-supercharge/jarvis/renovate-presets/-/issues) page.

---

# Versioning

This library is developed using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and versioned with [standard version](https://github.com/conventional-changelog/standard-version).
